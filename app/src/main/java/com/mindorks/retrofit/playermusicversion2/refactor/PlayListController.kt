package com.mindorks.retrofit.playermusicversion2.refactor

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.mindorks.retrofit.playermusicversion2.data.Song
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.MainScope

object PlayListController {
    private val _nextAble = MutableLiveData<Boolean>()
    private val _previousAble = MutableLiveData<Boolean>()
    private val _songs = MutableLiveData<List<Song>>(emptyList())
    private val _looping = MutableLiveData<Boolean>(false)
    private val _shuffling = MutableLiveData<Boolean>(false)
    private val _playing = MutableLiveData<Boolean>(false)
    private val _currenttime = MutableLiveData<Long>()
    private val _duration = MutableLiveData<Long>()
    private val _playingSong = MutableLiveData<Song>()
    private val _onError = MutableLiveData<Boolean>(false)

    val nextAble: LiveData<Boolean> get() = _nextAble
    val previousAble: LiveData<Boolean> get() = _previousAble
    val songs: LiveData<List<Song>> get() = _songs
    val looping: LiveData<Boolean> get() = _looping
    val shuffling: LiveData<Boolean> get() = _shuffling
    val playing: LiveData<Boolean> get() = _playing
    val currenttime: LiveData<Long> get() = _currenttime
    val duration: LiveData<Long> get() = _duration
    val playingSong: LiveData<Song> get() = _playingSong
    val onError: LiveData<Boolean> get() = _onError
    private var timeScope: CoroutineScope? = null

    fun addSong(song: Song) {
        val data = ArrayList(_songs.value ?: emptyList())
        data.add(song)
        _songs.value = data
    }

    fun removeSong(song: Song) {

    }

    fun pauseOrResume() {

    }

    fun next() {

    }

    fun previous() {

    }


    fun setLooping(isLooping: Boolean) {

    }

    fun setShuffling(isShuffling: Boolean) {

    }

    fun play(pos: Int) {

    }

    fun clear() {
        _songs.value = emptyList()
    }

    private fun getSong(pos: Int): Song? {
        return _songs.value?.getOrElse(pos) {
            null
        }
    }

    private fun size(): Int {
        return _songs.value?.size ?: 0
    }

}