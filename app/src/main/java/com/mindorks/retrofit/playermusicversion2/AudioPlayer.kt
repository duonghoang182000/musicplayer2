package com.mindorks.retrofit.musicplayer

import android.content.Context
import android.media.MediaPlayer
import android.net.Uri

class AudioPlayer  {
    val mediaPlayer : MediaPlayer = MediaPlayer()
    fun onPlay(context: Context, uri: Uri){
        mediaPlayer.reset()
        mediaPlayer.setDataSource(context, uri)
        mediaPlayer.prepare();
        mediaPlayer.setOnPreparedListener {
            mediaPlayer.setVolume(100F, 100F)
            mediaPlayer.start()
        }
    }

    fun onPause(){
        mediaPlayer.pause()
    }

    fun onStop(){
        mediaPlayer.stop()
    }

    fun onResume(){
        mediaPlayer.seekTo(onCurrentTime())
        mediaPlayer.start()
    }

    fun onDuration() : Int{
        return mediaPlayer.duration
    }

    fun onCurrentTime() : Int{
        return mediaPlayer.currentPosition
    }

    fun setToDuration(duration: Int) {
        mediaPlayer.seekTo(duration)
    }
}