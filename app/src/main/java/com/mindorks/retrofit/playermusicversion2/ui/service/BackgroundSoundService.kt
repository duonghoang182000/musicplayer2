package com.mindorks.retrofit.musicplayer.ui.service

import android.app.*
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.Color
import android.os.*
import android.support.v4.media.session.MediaSessionCompat
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import com.mindorks.retrofit.playermusicversion2.MainActivity
import com.mindorks.retrofit.playermusicversion2.MyReceiver
import com.mindorks.retrofit.playermusicversion2.R
import java.util.*

class BackgroundSoundService : Service() {
    override fun onBind(intent: Intent?): IBinder? {
        TODO("Not yet implemented")
    }
    // Binder given to clients
  /*  private val binder = LocalBinder()
    private lateinit var CHANNEL_ID: String
    private val ACTION_PAUSE = 1
    private val ACTION_RESUME = 2
    private val ACTION_PREVIOUS = 3
    private val ACTION_NEXT = 4


    *//**
     * Class used for the client Binder.  Because we know this service always
     * runs in the same process as its clients, we don't need to deal with IPC.
     *//*
    inner class LocalBinder : Binder() {
        // Return this instance of LocalService so clients can call public methods
        fun getService(): BackgroundSoundService = this@BackgroundSoundService
    }

    override fun onBind(intent: Intent): IBinder {
        return binder
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        val bundle = intent?.extras
        val actionMusic = intent?.getIntExtra("action_music_service", 0)
        handleActionMusic(actionMusic)
        sendNotification()
        return START_NOT_STICKY
    }

    private fun sendNotification() {
        val bitmap = BitmapFactory.decodeResource(resources, R.drawable.ic_music)
        val notificationIntent = Intent(this, MainActivity::class.java)
        notificationIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
        val pendingIntent = PendingIntent.getActivity(
            this,
            0,
            notificationIntent,
            PendingIntent.FLAG_UPDATE_CURRENT
        )
        CHANNEL_ID = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createNotificationChannel("my_service", "My Background Service")
        } else {
            // If earlier version channel ID is not used
            // https://developer.android.com/reference/android/support/v4/app/NotificationCompat.Builder.html#NotificationCompat.Builder(android.content.Context)
            ""
        }

        val notification = NotificationCompat.Builder(this, CHANNEL_ID)
            .setContentTitle(PlayListController.currentSong.value?.nameSong)
            .setContentText("hihi")
            .setSmallIcon(R.drawable.ic_music)
            .setLargeIcon(bitmap)
            .setContentIntent(pendingIntent)
            .setStyle(
                androidx.media.app.NotificationCompat.MediaStyle()
                    .setShowActionsInCompactView(0, 1, 2)
                    .setMediaSession(MediaSessionCompat(this, "tag").sessionToken)
            )


        if (PlayListController.audioPlayer.mediaPlayer.isPlaying) {
            notification
                .addAction(
                    R.drawable.previous_song, "Previous", getPendingIntent(
                        this,
                        ACTION_PREVIOUS
                    )
                )
                .addAction(R.drawable.ic_pause_song, "Pause", getPendingIntent(this, ACTION_PAUSE))
                .addAction(R.drawable.next_song, "Next", getPendingIntent(this, ACTION_NEXT))

        } else {
            notification
                .addAction(
                    R.drawable.previous_song, "Previous", getPendingIntent(
                        this,
                        ACTION_PREVIOUS
                    )
                )
                .addAction(R.drawable.ic_play, "Pause", getPendingIntent(this, ACTION_RESUME))
                .addAction(R.drawable.next_song, "Next", getPendingIntent(this, ACTION_NEXT))
        }

        startForeground(2, notification.build())
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationChannel(channelId: String, channelName: String): String {
        val chan = NotificationChannel(
            channelId,
            channelName, NotificationManager.IMPORTANCE_NONE
        )
        chan.lightColor = Color.BLUE
        chan.lockscreenVisibility = Notification.VISIBILITY_PRIVATE
        val service = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        service.createNotificationChannel(chan)
        return channelId
    }

    fun getPendingIntent(context: Context, action: Int): PendingIntent {
        val intent = Intent(this, MyReceiver::class.java)
        intent.putExtra("action_music", action)
        return PendingIntent.getBroadcast(
            context.applicationContext,
            action,
            intent,
            PendingIntent.FLAG_UPDATE_CURRENT
        )
    }

    fun handleActionMusic(action: Int?) {
        when (action) {
            ACTION_PAUSE -> {
                PlayListController.audioPlayer.onPause()
                PlayListController.isPlaying()
            }
            ACTION_RESUME -> {
                PlayListController.audioPlayer.onResume()
                PlayListController.isPlaying()
            }
            ACTION_PREVIOUS -> PlayListController.onPrevious()
            ACTION_NEXT -> PlayListController.onNext()
        }
    }*/
}