package com.mindorks.retrofit.playermusicversion2.screens.home

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.map
import com.mindorks.retrofit.playermusicversion2.data.toSong
import com.mindorks.retrofit.playermusicversion2.refactor.PlayListController


class HomeViewModel(application: Application) : AndroidViewModel(application) {
    private val _progress = MediatorLiveData<Int>()
    val progress: LiveData<Int> get() = _progress

    init {
        _progress.addSource(PlayListController.currenttime) {
            updateProgrees()
        }
        _progress.addSource(PlayListController.duration) {
            updateProgrees()
        }
    }

    private fun updateProgrees() {
        val currentTime = PlayListController.currenttime.value
        val duration = PlayListController.duration.value

        if (currentTime == null || duration == null) {
            return
        }

        if (duration == 0L) {
            return
        }
        _progress.value = (currentTime.toFloat() / duration.toFloat() * 100).toInt()

    }

    fun isPlaying(): LiveData<Boolean> {
        return PlayListController.playing
    }

    fun isLooping(): LiveData<Boolean> {
        return PlayListController.looping
    }

    fun isShuffling(): LiveData<Boolean> {
        return PlayListController.shuffling
    }

    fun isNextAble(): LiveData<Boolean> {
        return PlayListController.nextAble
    }

    fun isPreviousAble(): LiveData<Boolean> {
        return PlayListController.previousAble
    }

    fun currentTime(): LiveData<Long> {
        return PlayListController.currenttime
    }

    fun duration(): LiveData<Long> {
        return PlayListController.duration
    }


    fun songName(): LiveData<String> {
        return PlayListController.playingSong.map { it.nameSong }
    }

    fun onError(): LiveData<Boolean> {
        return PlayListController.onError
    }


    fun play(filePath: String) {
        PlayListController.clear()
        PlayListController.addSong(filePath.toSong())
        PlayListController.play(0)
    }

    fun clickedOnNextButton() {
        PlayListController.next()
    }

    fun clickedOnPreviousButton() {
        PlayListController.previous()
    }

    fun clickedOnShuffling() {
        PlayListController.setLooping(!(PlayListController.shuffling.value ?: false))
    }

    fun clickedOnLooping() {
        PlayListController.setLooping(!(PlayListController.looping.value ?: false))
    }
}
