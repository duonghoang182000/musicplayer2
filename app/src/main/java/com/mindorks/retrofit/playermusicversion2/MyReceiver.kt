package com.mindorks.retrofit.playermusicversion2

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.mindorks.retrofit.musicplayer.ui.service.BackgroundSoundService

class MyReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        val actionMusic = intent?.getIntExtra("action_music", 0)
        val intentService = Intent(context, BackgroundSoundService::class.java)
        intentService.putExtra("action_music_service", actionMusic)

        context?.startService(intentService)
    }
}