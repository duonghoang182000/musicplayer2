package com.mindorks.retrofit.playermusicversion2.data

fun String.toSong(): Song {
    return Song("test", this)
}

data class Song(
    var nameSong: String = "",
    var path: String = ""
)

