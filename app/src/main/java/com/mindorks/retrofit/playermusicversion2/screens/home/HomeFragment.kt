package com.mindorks.retrofit.playermusicversion2.screens.home

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SeekBar
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.mindorks.retrofit.musicplayer.ui.service.BackgroundSoundService
import com.mindorks.retrofit.playermusicversion2.MusicApplication
import com.mindorks.retrofit.playermusicversion2.R
import com.mindorks.retrofit.playermusicversion2.databinding.HomeFragmentBinding

fun Long.toTime(): String {
    return "nskan"
}

class HomeFragment : Fragment(), View.OnClickListener {

    companion object {
        private val ARG_FILE_PATH = "ARG_FILE_PATH"

        fun newInstance(filePath: String) = HomeFragment().apply {
            arguments = Bundle().apply {
                putString(ARG_FILE_PATH, filePath)
            }
        }
    }

    lateinit var binding: HomeFragmentBinding

    private val viewModel: HomeViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.home_fragment,
            container,
            false
        )

        observerData()
        bindEvents()
        return binding.root
    }

    private fun observerData() {
        viewModel.songName().observe(viewLifecycleOwner) {
            binding.tvNameSong2.text = it
        }
        viewModel.currentTime().observe(viewLifecycleOwner) {
            binding.tvDurationStart.text = it.toTime()
        }
        viewModel.duration().observe(viewLifecycleOwner) {
            binding.tvDurationEnd.text = it.toTime()
        }
        viewModel.progress.observe(viewLifecycleOwner) {
            binding.sbDuration.progress = it
        }

        viewModel.isPlaying().observe(viewLifecycleOwner) {
            if (it) {
                binding.ivStartSong2.setImageResource(R.drawable.ic_play)
            } else {
                binding.ivStartSong2.setImageResource(R.drawable.ic_pause_song)
            }
        }
    }

    private fun bindEvents() {
        binding.ivNextSong2.setOnClickListener(this)
        binding.ivPreSong2.setOnClickListener(this)
        binding.ivShuffle.setOnClickListener(this)
        binding.ivRepeat.setOnClickListener(this)
    }

    override fun onClick(view: View?) {
        when (view) {
            binding.ivNextSong2 -> viewModel.clickedOnNextButton()
            binding.ivPreSong2 -> viewModel.clickedOnPreviousButton()
            binding.ivShuffle -> viewModel.clickedOnShuffling()
            binding.ivRepeat -> viewModel.clickedOnLooping()

        }

    }
}