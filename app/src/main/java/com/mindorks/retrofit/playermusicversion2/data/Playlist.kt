package com.mindorks.retrofit.playermusicversion2.data

data class Playlist(
    val playlistId: Long = 0L,
    var playlistName: String = ""
)